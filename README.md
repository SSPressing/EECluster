EECluster is software tool for managing the energy-efficient allocation of the cluster resources. EECluster uses a Hybrid Genetic Fuzzy System as the decision-making mechanism that elicits part of its rule base dependent on the cluster workload scenario, delivering good compliance with the administrator preferences.

In the latest version, we leverage a more sophisticated and exhaustive model that covers a wider range of environmental aspects and balances service quality and power consumption with all indirect costs, including hardware failures and subsequent replacements, measured in both monetary units and carbon emissions.

This software has been developed by Quantum and High Performance Computing group (QHPC). See https://qhpc.uniovi.es/ and https://sourceforge.net/projects/eecluster/
